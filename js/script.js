//бургер
document.getElementById("trigger").onclick = function() {
    open()
  };
  
function open() {
	document.getElementById("nav").classList.toggle("show");
}

// анимация бургера
const menu = document.querySelector("svg");
menu.addEventListener("click", morph);

function morph() {
  menu.classList.toggle("open");
}

//плавная прокрутка от бургера к секции
$(document).ready(function(){
	$(".nav").on("click","a", function (event) {
		//отменяем стандартную обработку нажатия по ссылке
		event.preventDefault();

		//забираем идентификатор бока с атрибута href
		var id  = $(this).attr('href'),

		//узнаем высоту от начала страницы до блока на который ссылается якорь
			top = $(id).offset().top;
		
		//анимируем переход на расстояние - top за 1500 мс
		$('body,html').animate({scrollTop: top}, 1500);
	});
});

//плавная прокрутка от секции к бургеру
$(function() {

    $('.topNubex').click(function() {
        $('body,html').animate({scrollTop:0},500);
        return false;
    })

})